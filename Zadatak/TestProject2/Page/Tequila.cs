﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject2.Page
{
    public class Tequila
    {
        public IWebDriver driver;
        public Tequila(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public IWebElement text => driver.FindElement(By.XPath("/html/body/div[1]/div[2]/div/article/div/div/div/div/div[2]/div/div[1]/div[1]/div/h1"));

        public IWebElement search => driver.FindElement(By.XPath("/html/body/div[1]/header/div[1]/div[2]/div[1]/span"));

        public IWebElement chili => driver.FindElement(By.ClassName("et-search-field"));
        public string TextSelect() => text.Text;

        public void SearchClick()
        {
            Actions action = new Actions(driver);
            action.MoveToElement(search).Click().Perform();
        }
        public void ClearBar() => chili.Clear();
        public void SendChili() => chili.SendKeys("CHILI");
        public void Enter() => chili.SendKeys(Keys.Enter);
    }
}
