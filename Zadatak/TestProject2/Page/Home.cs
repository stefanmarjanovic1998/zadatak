﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Collections.Generic;
using System.Text;

namespace TestProject2.Page
{
    public class Home
    {
        public IWebDriver driver;
        public Home(IWebDriver webDriver)
        {
            driver = webDriver;
        }

        public IWebElement products => driver.FindElement(By.Id("menu-item-3830"));
        public IWebElement tequila => driver.FindElement(By.Id("menu-item-20424"));
        public void Hover()
        {
            Actions action = new Actions(driver);
            action.MoveToElement(products).Perform();
        }

        public void TequilaClick() => tequila.Click();
    }
}
