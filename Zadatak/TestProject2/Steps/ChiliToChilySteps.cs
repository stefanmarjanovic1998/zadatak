﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using TestProject2.Page;

namespace TestProject2.Steps
{
    [Binding]
    public class SkyWalkSteps
    {
        public IWebDriver driver = new ChromeDriver();
        public Chili chili = null;

        [Given(@"I am on a CHILY page")]
        public void GivenIAmOnACHILYPage()
        {
            driver.Navigate().GoToUrl("https://skywalk.info/?s=CHILI");
            chili = new Chili(driver);
        }
        
        [Given(@"I click on a search bar of a CHILY page")]
        public void GivenIClickOnASearchBarOfACHILYPage()
        {
            chili.SearchClick();
        }
        
        [Given(@"I search for a CHILY")]
        public void GivenISearchForACHILY()
        {
            chili.ClearBar();
            chili.SendChily();
            chili.Enter();
        }
        
        [Then(@"I should be on CHILY URL")]
        public void ThenIShouldBeOnCHILYURL()
        {
            Assert.AreEqual(driver.Url, "https://skywalk.info/?s=CHILY");
        }
        
        [Then(@"I should have No Results Found text on a CHILY page")]
        public void ThenIShouldHaveNoResultsFoundTextOnACHILYPage()
        {
            Assert.True(driver.PageSource.Contains("No Results Found"));
        }
    }
}
