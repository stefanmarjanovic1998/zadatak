﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using TechTalk.SpecFlow;
using TestProject2.Page;

namespace TestProject2.Steps
{
    
    [Binding]
    public class BTequilaToChiliSteps
    {
        public IWebDriver driver = new ChromeDriver();
        public Tequila tequila = null;

        [Given(@"I am on a CHILI page")]
        public void GivenIAmOnANewPage()
        {
            driver.Navigate().GoToUrl("https://skywalk.info/project/tequila/");
            tequila = new Tequila(driver);
        }

        [Given(@"I click on a search bar of a CHILI page")]
        public void GivenIClickOnASearchBar()
        {
            tequila.SearchClick();
        }
        
        [Given(@"I search for a CHILI")]
        public void GivenISearchForACHILI()
        { 
            tequila.SendChili();
            tequila.Enter();
            tequila.ClearBar();
        }

        [Then(@"I should be on CHILI URL")]
        public void ThenIShouldBeOnNewURL()
        {
            Assert.AreEqual(driver.Url, "https://skywalk.info/?s=CHILI");
        }

        [Then(@"I should have selected text on a CHILI page")]
        public void ThenIShouldSeeSelectedText()
        {
            Assert.True(driver.PageSource.Contains("CHILI4 – Limited Design – “Yellow”"));
        }
    }
}
