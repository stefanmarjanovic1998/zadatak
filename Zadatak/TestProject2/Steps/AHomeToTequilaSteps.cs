﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using TechTalk.SpecFlow;
using TestProject2.Page;

namespace TestProject2.Steps
{
    [Binding]
    public class AHomeToTequilaSteps
    {
        public IWebDriver driver = new ChromeDriver();
        Home home = null;

        [Given(@"I launch the application")]
        public void GivenILaunchTheApplication()
        {
            driver.Navigate().GoToUrl("https://skywalk.info/");
            home = new Home(driver);
            
        }
        
        [Given(@"I click on TEQUILA in Products menu")]
        public void GivenIClickOnTequila()
        {
            home.Hover();
            home.TequilaClick();
        }
        
        [Then(@"I should be on Tequila URL")]
        public void ThenIShouldSeeEmployeeDetailsLink()
        {
            Assert.AreEqual(driver.Url, "https://skywalk.info/project/tequila/");
        }

        [Then(@"I should have selected text on a Tequila page")]
        public void ThenIShouldSeeSelectedText()
        {
            Assert.True(driver.PageSource.Contains("Free your mind. For the essentials."));
        }
    }
}
