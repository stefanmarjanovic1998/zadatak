﻿Feature: SkyWalk
	Testing SkyWalk page

@tequila
Scenario: APerform going to a Tequila page of SkyWalk site 
	Given I launch the application
	And I click on TEQUILA in Products menu
	Then I should be on Tequila URL
	Then I should have selected text on a Tequila page

@chili
Scenario: BPerform going to a CHILI page of SkyWalk site 
	Given I am on a CHILI page
	Given I click on a search bar of a CHILI page
	And I search for a CHILI
	Then I should be on CHILI URL
	Then I should have selected text on a CHILI page

@chily
Scenario: CPerform going to a CHILY page of SkyWalk site 
	Given I am on a CHILY page
	Given I click on a search bar of a CHILY page
	And I search for a CHILY
	Then I should be on CHILY URL
	Then I should have No Results Found text on a CHILY page